# CMake generated Testfile for 
# Source directory: /media/nvidia/D216D77016D753D7/drone_ws/src/mavros/libmavconn
# Build directory: /media/nvidia/D216D77016D753D7/drone_ws/build/libmavconn
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_libmavconn_gtest_mavconn-test "/media/nvidia/D216D77016D753D7/drone_ws/build/libmavconn/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/media/nvidia/D216D77016D753D7/drone_ws/build/libmavconn/test_results/libmavconn/gtest-mavconn-test.xml" "--return-code" "/media/nvidia/D216D77016D753D7/drone_ws/devel/.private/libmavconn/lib/libmavconn/mavconn-test --gtest_output=xml:/media/nvidia/D216D77016D753D7/drone_ws/build/libmavconn/test_results/libmavconn/gtest-mavconn-test.xml")
subdirs(gtest)
