# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/media/nvidia/D216D77016D753D7/drone_ws/build/libmavconn/catkin_generated/src/mavlink_helpers.cpp" "/media/nvidia/D216D77016D753D7/drone_ws/build/libmavconn/CMakeFiles/mavconn.dir/catkin_generated/src/mavlink_helpers.cpp.o"
  "/media/nvidia/D216D77016D753D7/drone_ws/src/mavros/libmavconn/src/interface.cpp" "/media/nvidia/D216D77016D753D7/drone_ws/build/libmavconn/CMakeFiles/mavconn.dir/src/interface.cpp.o"
  "/media/nvidia/D216D77016D753D7/drone_ws/src/mavros/libmavconn/src/serial.cpp" "/media/nvidia/D216D77016D753D7/drone_ws/build/libmavconn/CMakeFiles/mavconn.dir/src/serial.cpp.o"
  "/media/nvidia/D216D77016D753D7/drone_ws/src/mavros/libmavconn/src/tcp.cpp" "/media/nvidia/D216D77016D753D7/drone_ws/build/libmavconn/CMakeFiles/mavconn.dir/src/tcp.cpp.o"
  "/media/nvidia/D216D77016D753D7/drone_ws/src/mavros/libmavconn/src/udp.cpp" "/media/nvidia/D216D77016D753D7/drone_ws/build/libmavconn/CMakeFiles/mavconn.dir/src/udp.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROS_BUILD_SHARED_LIBS=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/media/nvidia/D216D77016D753D7/drone_ws/src/mavros/libmavconn/include"
  "catkin_generated/include"
  "/media/nvidia/D216D77016D753D7/drone_ws/devel/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
