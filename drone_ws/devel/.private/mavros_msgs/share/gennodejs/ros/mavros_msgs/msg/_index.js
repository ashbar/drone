
"use strict";

let HilStateQuaternion = require('./HilStateQuaternion.js');
let ActuatorControl = require('./ActuatorControl.js');
let ADSBVehicle = require('./ADSBVehicle.js');
let Altitude = require('./Altitude.js');
let AttitudeTarget = require('./AttitudeTarget.js');
let BatteryStatus = require('./BatteryStatus.js');
let CamIMUStamp = require('./CamIMUStamp.js');
let CommandCode = require('./CommandCode.js');
let CompanionProcessStatus = require('./CompanionProcessStatus.js');
let DebugValue = require('./DebugValue.js');
let ExtendedState = require('./ExtendedState.js');
let FileEntry = require('./FileEntry.js');
let GlobalPositionTarget = require('./GlobalPositionTarget.js');
let HilActuatorControls = require('./HilActuatorControls.js');
let HilControls = require('./HilControls.js');
let HilGPS = require('./HilGPS.js');
let HilSensor = require('./HilSensor.js');
let HomePosition = require('./HomePosition.js');
let LandingTarget = require('./LandingTarget.js');
let LogData = require('./LogData.js');
let LogEntry = require('./LogEntry.js');
let ManualControl = require('./ManualControl.js');
let Mavlink = require('./Mavlink.js');
let MountControl = require('./MountControl.js');
let OnboardComputerStatus = require('./OnboardComputerStatus.js');
let OpticalFlowRad = require('./OpticalFlowRad.js');
let OverrideRCIn = require('./OverrideRCIn.js');
let Param = require('./Param.js');
let ParamValue = require('./ParamValue.js');
let PositionTarget = require('./PositionTarget.js');
let RadioStatus = require('./RadioStatus.js');
let RCIn = require('./RCIn.js');
let RCOut = require('./RCOut.js');
let RTCM = require('./RTCM.js');
let State = require('./State.js');
let StatusText = require('./StatusText.js');
let Thrust = require('./Thrust.js');
let TimesyncStatus = require('./TimesyncStatus.js');
let Trajectory = require('./Trajectory.js');
let VehicleInfo = require('./VehicleInfo.js');
let VFR_HUD = require('./VFR_HUD.js');
let Vibration = require('./Vibration.js');
let Waypoint = require('./Waypoint.js');
let WaypointList = require('./WaypointList.js');
let WaypointReached = require('./WaypointReached.js');
let WheelOdomStamped = require('./WheelOdomStamped.js');

module.exports = {
  HilStateQuaternion: HilStateQuaternion,
  ActuatorControl: ActuatorControl,
  ADSBVehicle: ADSBVehicle,
  Altitude: Altitude,
  AttitudeTarget: AttitudeTarget,
  BatteryStatus: BatteryStatus,
  CamIMUStamp: CamIMUStamp,
  CommandCode: CommandCode,
  CompanionProcessStatus: CompanionProcessStatus,
  DebugValue: DebugValue,
  ExtendedState: ExtendedState,
  FileEntry: FileEntry,
  GlobalPositionTarget: GlobalPositionTarget,
  HilActuatorControls: HilActuatorControls,
  HilControls: HilControls,
  HilGPS: HilGPS,
  HilSensor: HilSensor,
  HomePosition: HomePosition,
  LandingTarget: LandingTarget,
  LogData: LogData,
  LogEntry: LogEntry,
  ManualControl: ManualControl,
  Mavlink: Mavlink,
  MountControl: MountControl,
  OnboardComputerStatus: OnboardComputerStatus,
  OpticalFlowRad: OpticalFlowRad,
  OverrideRCIn: OverrideRCIn,
  Param: Param,
  ParamValue: ParamValue,
  PositionTarget: PositionTarget,
  RadioStatus: RadioStatus,
  RCIn: RCIn,
  RCOut: RCOut,
  RTCM: RTCM,
  State: State,
  StatusText: StatusText,
  Thrust: Thrust,
  TimesyncStatus: TimesyncStatus,
  Trajectory: Trajectory,
  VehicleInfo: VehicleInfo,
  VFR_HUD: VFR_HUD,
  Vibration: Vibration,
  Waypoint: Waypoint,
  WaypointList: WaypointList,
  WaypointReached: WaypointReached,
  WheelOdomStamped: WheelOdomStamped,
};
