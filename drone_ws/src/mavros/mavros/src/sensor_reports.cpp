#include <ros/ros.h>
#include <ti_mmwave_rrospkg/RadarScan.h>

#define RANGE_THRESHOLD 2.0

//Globals
ros::Rate *rate;
ti_mmwave::RadarScan radar_msg;
ti_mmwave::Object obj;
//Subsciber Clients
ros::Subscriber radar_cl;
ros::Publisher object_detected_pub;


//msg rcv callbacks
void radar_cb(const ti_mmwave::ConstPtr& msg)
{
    radar_msg = *msg;
}

int main(int argc, char **argv)
{
   ros::init(argc, argv, "master");
   ros::NodeHandle nh;

    radar_cl   = nh.subscribe<RadarScan>
                    ("ti_mmwave/radar_scan", 10);
        
    target_alt_reached_pub = nh.advertise<ti_mmwave::Object>
                    ("ti_mmwave/object_detected", 10);


    rate = new ros::Rate(20.0);    

    while(ros::ok())
    {   
        if(radar_msg.range > RANGE_THRESHOLD)
        {   
            obj.ObjectinRange = true;
            target_alt_reached_pub.publish(obj);
        }
        ros::spinOnce();
        rate->sleep();
    }
}
    

    