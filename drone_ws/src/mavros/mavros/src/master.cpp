#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/IsTargetAltReach.h>

//Globals
ros::Rate *rate;
mavros_msgs::IsTargetAltReach IsAltReached;
mavros_msgs::State current_state;
mavros_msgs::SetMode offb_set_mode;

//Subscriber clients
ros::Subscriber targetAlt_cl;
ros::Subscriber state_sub;

//Service Clients
ros::ServiceClient set_mode_client;

//Publisher Clients
ros::Publisher local_pos_pub;


//func cb
void msg_cb_1(const mavros_msgs::IsTargetAltReach::ConstPtr& msg)
{
    IsAltReached = *msg;
}

void state_cb(const mavros_msgs::State::ConstPtr& msg)
{
    current_state = *msg;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "master");
    ros::NodeHandle nh;
    
    state_sub       = nh.subscribe<mavros_msgs::State>
                        ("mavros/state", 10, state_cb);

    targetAlt_cl    = nh.subscribe<mavros_msgs::IsTargetAltReach>
                        ("myTopicNiraj", 10,msg_cb_1);
                
    set_mode_client = nh.serviceClient<mavros_msgs::SetMode>
                        ("mavros/set_mode");

    local_pos_pub   = nh.advertise<geometry_msgs::PoseStamped>
                        ("mavros/setpoint_position/local", 10);
    

    rate = new ros::Rate(20.0);    

    while(ros::ok())
    {   
        if(IsAltReached.TargetAltAcheived)
        {
            ROS_INFO("READY TO TAKE CMDS");
            break;
        }
        ros::spinOnce();
        rate->sleep();
    }
    
    geometry_msgs::PoseStamped pose;
    pose.pose.position.x = 0;
    pose.pose.position.y = 0;
    pose.pose.position.z = 2;
    
    ros::Time last_request = ros::Time::now();
    //Change to Auto-Mode for WP mission.
    	while(ros::ok())
	{
		if( current_state.mode != "AUTO" && (ros::Time::now() - last_request > ros::Duration(5.0)))
       		 {       
            		offb_set_mode.request.custom_mode = "AUTO";
            		if( set_mode_client.call(offb_set_mode) && offb_set_mode.response.mode_sent)
            		{
                		ROS_INFO("AUTO  enabled");
            		}
            		last_request = ros::Time::now();
        	} 
	

        // ROS_INFO("Current Alt - %02.f",&height_show);

	       
        local_pos_pub.publish(pose);

        ros::spinOnce();
        rate->sleep();
	
	}
}